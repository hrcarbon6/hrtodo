# __init__.py

import sys

from tkinter import *
from hrtodo.data import *

# main window
class HRTodo(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)

        self.dataManagement = Data()

        self.todoDict = self.dataManagement.readData()
        
        self.todoList = list()
        
        self.createWidgets()
        self.grid()

    # create widget
    def createWidgets(self):
        #title
        self.titleLabel = Label(self, text="HRTodo")
        self.titleLabel.grid(row=0, column=0, columnspan=2)

        # todo list box
        self.todoListBox = Listbox(self)
        self.todoListBox.grid(row=1, column=0, columnspan=2)
        self.todoListBox.bind("<Double-1>", self.editEvent)

        self.loadData()

        self.todoListBox.config(selectmode = EXTENDED)
        self.todoListBox.select_set(0)

        # create button
        self.createButton = Button(self, text="Create", command=self.createEvent)
        self.createButton.grid(row=2, column=0)

        # delete button
        self.deleteButton = Button(self, text="Delete", command=self.deleteEvent)
        self.deleteButton.grid(row=2, column=1)

    # create event
    def createEvent(self):
        self.app = HRTodoCreateEventDialog(root)
        root.wait_window(self.app.top)
        self.loadData()

    # delete event
    def deleteEvent(self):
        for self.event in self.todoListBox.curselection():
            del self.todoDict[self.todoList[self.event - 0]]
            del self.todoList[self.event - 0]
            
            self.dataManagement.writeData(self.todoDict)

        self.loadData()

    # load data
    def loadData(self):

        self.todoListBox.delete(0, END)

        for self.todoItem in self.todoDict:
            self.todoList.append(self.todoItem)
        
        self.todoDict = self.dataManagement.readData()

        if self.todoDict == None:
            pass
        else:
            for self.todo in self.todoDict:
                self.todoListBox.insert(END, " {0}".format(self.todo))

    #edit event
    def editEvent(self, event):
        self.app = HRTodoEditEventDialog(root, self.todoListBox.curselection()[0])
        root.wait_window(self.app.top)
        self.loadData()

# edit event dialog
class HRTodoEditEventDialog:
    # initialize class
    def __init__(self, parent, itemID):
        self.top = Toplevel(parent)

        self.itemID = itemID

        self.dataManagement = Data()

        self.todoItem = list()
        for self.item in self.dataManagement.readData():
            self.todoItem.append(self.item)

        self.createWidgets()

    # create widgets
    def createWidgets(self):
        self.eventTextFieldLabel = Label(self.top, text="Event Name : ")
        self.eventTextFieldLabel.grid(row=0, column=0)
        
        self.eventTextField = Entry(self.top, width=20)
        self.eventTextField.grid(row=0,column=1, columnspan=3)
        self.eventTextField.insert(0, self.todoItem[self.itemID])

        self.descriptionTextViewLabel = Label(self.top, text="Description : ")
        self.descriptionTextViewLabel.grid(row=1, column=0)
        
        self.descriptionTextView = Text(self.top, width=20, height=10)
        self.descriptionTextView.grid(row=1, column=1, columnspan=3)
        self.descriptionTextView.insert("1.0", self.dataManagement.readData()[self.todoItem[self.itemID]])

        self.createButton = Button(self.top, text="Edit", command=self.editEvent)
        self.createButton.grid(row=2, column=0, columnspan=2)

        self.cancelButton = Button(self.top, text="Cancel", command=self.top.destroy)
        self.cancelButton.grid(row=2, column=2, columnspan=2)
        
    # edit event
    def editEvent(self):
        self.data = self.dataManagement.readData()
        
        del self.data[self.todoItem[self.itemID]]
        
        self.data[self.eventTextField.get()] = self.descriptionTextView.get("1.0", END)
        
        self.dataManagement.writeData(self.data)

        self.top.destroy()

# create event class
class HRTodoCreateEventDialog:
    # initialize class
    def __init__(self, parent):
        self.top = Toplevel(parent)

        self.dataManagement = Data()

        self.createWidgets()

    # create widgets
    def createWidgets(self):
        self.eventTextFieldLabel = Label(self.top, text="Event Name : ")
        self.eventTextFieldLabel.grid(row=0, column=0)
        
        self.eventTextField = Entry(self.top, width=20)
        self.eventTextField.grid(row=0,column=1, columnspan=3)

        self.descriptionTextViewLabel = Label(self.top, text="Description : ")
        self.descriptionTextViewLabel.grid(row=1, column=0)
        
        self.descriptionTextView = Text(self.top, width=20, height=10)
        self.descriptionTextView.grid(row=1, column=1, columnspan=3)

        self.createButton = Button(self.top, text="Create", command=self.createEvent)
        self.createButton.grid(row=2, column=0, columnspan=2)

        self.cancelButton = Button(self.top, text="Cancel", command=self.top.destroy)
        self.cancelButton.grid(row=2, column=2, columnspan=2)

    # create event
    def createEvent(self):
        if isinstance(self.dataManagement.readData(), dict):
            self.data = self.dataManagement.readData()
            self.data[self.eventTextField.get()] = self.descriptionTextView.get("1.0", END)
            self.dataManagement.writeData(self.data)
        else:
            self.data[self.eventTextField.get()] = self.descriptionTextView.get("1.0", END)
            self.dataManagement.writeData(self.data)
        
        self.top.destroy()
        

root = Tk()
root.title("HRTodo")
root.resizable(width=False, height=False)
app = HRTodo(master=root)
app.mainloop()
