# data.py

import os
import pickle

class Data:
    def __init__(self):
        self.path = os.path.expanduser("~/.todo.data")
        if os.path.exists(self.path):
            pass
        else:
            self.writeData(dict())

    def writeData(self, data):
        with open(self.path, "wb") as f:
            pickle.dump(data, f)

    def readData(self):
        self.data = None
        with open(self.path, "rb") as f:
            self.data = pickle.load(f)

        return self.data
