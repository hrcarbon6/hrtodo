# test.py

import os
import sys

from data import *

def showData():

    data = Data().readData()

    print(data)
    print(type(data))

def clearData():
    os.system("rm -rf ~/.todo.data")

while True:
    mode = input("Please input mode> ")
    
    if mode == "s":
        showData()
    elif mode == "c":
        clearData()
    elif mode == "q":
        break
    else:
        pass

sys.exit()
